package de.ddkfm.application;
	

import java.util.Optional;


import de.ddkfm.views.FieldNode;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class Main extends Application {
	private Label lblHeading = new Label("Langtons Ameise(zellul�rer Automat)");
	private Label lblIterations = new Label("Iterationen: ");
	private Button btNextRound = new Button("N�chste Runde");
	private Button btIteration = new Button("Iterationen");
	private Button btReset = new Button("Reset");
	private TextField edStartX = new TextField("");
	private TextField edStartY = new TextField("");
	private ComboBox<String> cbDirection = new ComboBox<>();
	private Button btAddAnt = new Button("Ameise hinzuf�gen");
	private Button btRemoveAnt = new Button("Ameise l�schen");
	private Label lblAnts = new Label("aktuelle Anzahl an Ameisen: 0");
	
	private int iter = 0;
	
	private FieldNode mainField = new FieldNode(500, 500, 50, 50);
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = new BorderPane();
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			lblHeading.getStyleClass().add("header");
			cbDirection.getItems().addAll("Norden","Osten","S�den","Westen","Alle");
			cbDirection.getSelectionModel().select(0);
			root.setTop(lblHeading);
			
			
			root.setCenter(mainField);
			
			root.setRight(new VBox(
							btReset,btNextRound,new HBox(btIteration,lblIterations),
							lblAnts,
							new HBox(new Label("Startwert X: "), edStartX),
							new HBox(new Label("Startwert Y: "), edStartY),
							new HBox(new Label("Ausgangsrichtung "),cbDirection),
							new HBox(btAddAnt,btRemoveAnt)
							)
						 );
			
			btAddAnt.setOnAction(e->{
				int startX = mainField.getLogic().getSizeX() / 2;
				int startY = mainField.getLogic().getSizeY() / 2;
				int direction = 0;
				try {
					startX = Integer.parseInt(edStartX.getText());
					startY = Integer.parseInt(edStartY.getText());
					direction = cbDirection.getSelectionModel().getSelectedIndex();
				} catch (NumberFormatException ex) {
					System.out.println("Fehlerhafte Zahleneingabe");
				}
				if (direction < 4)
					mainField.addAnt(startX, startY, direction);
				else
					for(int i = 0;i<4;i++)
						mainField.addAnt(startX, startY, i);
				showAnts();
			});
			btRemoveAnt.setOnAction(e->{
				mainField.removeAnt();
				showAnts();
			});
			btNextRound.setOnAction(e->{
				mainField.nextStep();
			});
			btReset.setOnAction(e->{
				TextInputDialog dialog = new TextInputDialog("50");
				dialog.setContentText("Feldgr��e");
				Optional<String> result = dialog.showAndWait();
				
				if(result.isPresent()){
					int size = 50;
					try {
						size = Integer.parseInt(result.get());
					} catch (NumberFormatException ex) {
						Alert al = new Alert(AlertType.ERROR);
						al.setContentText("Die von Ihnen eingegebene Zahl ist keine g�ltige Ganzzahl!\n"
								+ "Der Wert wird auf 50 gesetzt");
						al.show();
					}
					mainField = new FieldNode(500,500,size,size);
					root.setCenter(mainField);
					showAnts();
				}
			});
			btIteration.setOnAction(e->{
				Timeline timer = new Timeline(
					new KeyFrame(Duration.millis(5),
						new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								mainField.nextStep();
								iter++;
								lblIterations.setText("Iterationen: " + iter);
							}
						}));
				
				TextInputDialog dialog = new TextInputDialog("10");
				dialog.setContentText("Anzahl der Iterationen");
				Optional<String> result = dialog.showAndWait();
				if(result.isPresent()){
					int count = 10;
					try {
						count = Integer.parseInt(result.get());
						timer.setCycleCount(count);
						timer.play();
					} catch (NumberFormatException ex) {
						dialog.close();
					}
				}
			});
			primaryStage.setScene(scene);
			primaryStage.setTitle("Langtons Ameisen - Demo");
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	private void showAnts(){
		int countOfAnts = mainField.getCountOfAnts();
		lblAnts.setText("aktuelle Anzahl an Ameisen: " + countOfAnts);
	}
	public static void main(String[] args) {
		launch(args);
	}
}
