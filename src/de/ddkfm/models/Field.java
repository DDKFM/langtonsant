package de.ddkfm.models;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class Field {
	private int gridSizeX;
	private int gridSizeY;
	private BooleanProperty[][] grid;
	public Field(int sizeX, int sizeY) {
		gridSizeX = sizeX;
		gridSizeY = sizeY;
		grid = new SimpleBooleanProperty[sizeX][sizeY];
		for(int x = 0;x<grid.length;x++)
			for(int y = 0;y<grid[x].length;y++)
				grid[x][y] = new SimpleBooleanProperty(false);
	}
	public void setCell(int x, int y, boolean value){
		int newX, newY;
		newX = (x <= -1)
				?gridSizeX-1
				:((x>=gridSizeX)
					?gridSizeX-1
					:x
				  );
		newY = (y <= -1)
				?gridSizeY-1
				:((y>=gridSizeY)
					?gridSizeY-1
					:y
				  );
		grid[newX][newY].set(value);
	}
	public boolean getCell(int x, int y){
		int newX, newY;
		newX = (x <= -1)
				?gridSizeX-1
				:((x>=gridSizeX)
					?gridSizeX-1
					:x
				  );
		newY = (y <= -1)
				?gridSizeY-1
				:((y>=gridSizeY)
					?gridSizeY-1
					:y
				  );
		return grid[newX][newY].get();
	}
	public int getSizeX(){
		return gridSizeX;
	}
	public int getSizeY(){
		return gridSizeY;
	}
}
