package de.ddkfm.models;

import javafx.geometry.Orientation;

public class Ant {
	
	public static final int NORTH = 0;
	public static final int EAST = 1;
	public static final int SOUTH = 2;
	public static final int WEST = 3;
	
	
	private int currentX;
	private int currentY;
	private Field field;
	private int direction;
	
	public Ant(Field field, int startX, int startY, int direction) {
		this.field = field;
		currentX = startX;
		currentY = startY;
		this.direction = direction;
	}
	public void nextStep(){
		int nextX = currentX;
		int nextY = currentY;
		switch(direction){
			case 0:nextY--;break;
			case 1:nextX++;break;
			case 2:nextY++;break;
			case 3:nextX--;break;
		}
		boolean nextValue = field.getCell(nextX, nextY);
		if(nextValue)
			direction = direction==0?3:(direction-1);
		else
			direction = (direction + 1)%4;
		currentX = nextX;
		currentY = nextY;
		field.setCell(currentX, currentY, !field.getCell(currentX, currentY));
	}
}
