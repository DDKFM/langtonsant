package de.ddkfm.views;

import java.util.Vector;

import de.ddkfm.models.Ant;
import de.ddkfm.models.Field;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class FieldNode extends Pane{
	private double fitSizeX;
	private double fitSizeY;
	private Field logic;
	private Vector<Ant> ants = new Vector<>();
	
	private Rectangle[][] cells;
	
	public FieldNode(double fitSizeX, double fitSizeY,int fieldX, int fieldY){
		this.fitSizeX = fitSizeX;
		this.fitSizeY = fitSizeY;
		logic = new Field(fieldX, fieldY);
		cells = new Rectangle[fieldX][fieldY];
		for(int x = 0;x<cells.length;x++)
			for(int y = 0;y<cells[x].length;y++){
				cells[x][y] = new Rectangle();
				cells[x][y].setWidth(fitSizeX / fieldX);
				cells[x][y].setHeight(fitSizeY / fieldY);
				cells[x][y].setLayoutX(x*fitSizeX / fieldX);
				cells[x][y].setLayoutY(y*fitSizeY / fieldY);
				if(logic.getCell(x, y))
					cells[x][y].setFill(Color.BLACK);
				else
					cells[x][y].setFill(Color.WHITE);
				this.getChildren().add(cells[x][y]);
			}
	}
	public void nextStep(){
		for(Ant ant : ants)
			ant.nextStep();
		loadFields();
	}
	private void loadFields(){
		for(int x = 0;x<cells.length;x++)
			for(int y = 0;y<cells[x].length;y++)
				if(logic.getCell(x, y))
					cells[x][y].setFill(Color.BLACK);
				else
					cells[x][y].setFill(Color.WHITE);
	}
	public void addAnt(int startX,int startY, int direction){
		ants.addElement(new Ant(logic, startX, startY, direction));
	}
	public void removeAnt(){
		ants.remove(ants.size()-1);
	}
	public Field getLogic(){
		return logic;
	}
	public int getCountOfAnts(){
		return ants.size();
	}
}
